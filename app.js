var model = [
  {
    id:1,
    name: 'Bajaj Pulsar 150 UG 4.5',
    color:'Red',
    price:174900,
    model:'2018'
  },
  {
    id:2,
    name: 'Bajaj Pulsar 160 NS',
    color:'Blue',
    price:199500,
    model:'2018'
  },
  {
    id:3,
    name: 'Bajaj Avenger 150 Street',
    color:'Red',
    price:199500,
    model:'2018'
  },
  { id:4,
    name: 'Bajaj V-15',
    color:'Red',
    price:159500,
    model:'2018'
  }
];

function doSomething(model, callback){
  var dataLength = model.length  

    for(var i = 0; i < dataLength; i++){

    // var vat = model.map(function(el){
    //   if (el.price === el.price) {
    //     return el.price*15/115
    //   }
    // })

    callback(model[i]);
  }
}

doSomething(model, function(data){
  if(data.id === 1 ){    
    var vat = data.price*100/115
    console.log('You buy '+ data.name+' '+ 'Price is '+ data.price + ' Model is '+ data.model+' '+ 'VAT is '+ vat );
  }
});

doSomething(model, function(data){
  if(data.id === 2 ){
    var vat = data.price*100/115
    console.log('You buy '+ data.name+' '+ 'Price is '+ data.price + ' Model is '+ data.model+' '+ 'VAT is '+ vat );
  }
});

doSomething(model, function(data){
  if(data.id === 3 ){
    var vat = data.price*100/115
    console.log('You buy '+ data.name+' '+ 'Price is '+ data.price + ' Model is '+ data.model+' '+ 'VAT is '+ vat );
  }
});

doSomething(model, function(data){  
  if(data.id === 4 ){
    var vat = data.price*100/115
    console.log('You buy '+ data.name+' '+ 'Price is '+ data.price + ' Model is '+ data.model+' '+ 'VAT is '+ vat );
  }
});